import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { LayoutComponent } from './layout.component';
import { LayoutRoutingModule } from './layout-routing.module';

@NgModule({
  declarations: [
    LayoutComponent
  ],
  imports: [
    BrowserModule,
    LayoutRoutingModule,
    CommonModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [LayoutComponent]
})
export class LayoutModule { }
