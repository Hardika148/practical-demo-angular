import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommondataService } from 'src/app/services/common_data.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { UpdateComponent } from '../update/update.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  getData: any;
  public bsModalRef: BsModalRef;
  constructor(
    private router: Router,
    public commondataService: CommondataService,
    private modalService: BsModalService,
  ) { }

  ngOnInit(): void {
    this.getList();
  }

  getList() {
    this.commondataService.getListData()
      .subscribe((res: any) => {
        console.log(res);
        if (res) {
          this.getData = res
        }
      }, err => {
        console.log(err);
      });
  }

  updateData(id){
    console.log(id);
    this.router.navigate(['/update' ,id])
  }  
}
