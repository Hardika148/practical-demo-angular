import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CommondataService } from 'src/app/services/common_data.service';


@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.scss']
})
export class UpdateComponent implements OnInit {

  updateModel : any;
  data: string;
  

  constructor(public commondataService: CommondataService,
              private route: ActivatedRoute,
  ) { 
    this.data = this.route.snapshot.paramMap.get('id');
    console.log(this.data);
  }

  ngOnInit(): void {
  }

  sendRequest(){

  }
}
