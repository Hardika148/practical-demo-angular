import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UpdateComponent } from './update.component';
import { UpdateRoutingModule } from './update-routing.module';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [UpdateComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    UpdateRoutingModule,
    FormsModule,
    BrowserModule
  ]
})
export class UpdateModule { }
