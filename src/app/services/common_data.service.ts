import * as _ from 'lodash';
import { HttpClient } from '@angular/common/http';
import { Injectable} from '@angular/core';

@Injectable()

export class CommondataService {
    
    constructor(public httpClient: HttpClient) {  }

   getListData() {
    return this.httpClient.get('https://jsonplaceholder.typicode.com/posts')
  }

}
